# IntelliJ-Guide

I will do more with this guide when I get a chance

1. [Setting up JDBC](https://bitbucket.org/TheLazyHatGuy/intellij-guide/wiki/Setting%20up%20JDBC)

2. [Useful Shortcuts](https://bitbucket.org/TheLazyHatGuy/intellij-guide/wiki/Useful%20shortcuts)

3. [Add a library to a Gradle Project](https://bitbucket.org/TheLazyHatGuy/intellij-guide/wiki/Add%20a%20library%20to%20a%20Gradle%20project)

4. [Add a library to a Maven Project](https://bitbucket.org/TheLazyHatGuy/intellij-guide/wiki/Add%20a%20library%20to%20a%20Maven%20Project)